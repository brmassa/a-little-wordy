# A Little Wordy

An game by **Bruno Massa** from the board game from [Exploding Kittens](https://www.explodingkittens.com/) of the [same name](https://www.explodingkittens.com/products/a-little-wordy).

## Tech

[Angular](https://angularjs.org/) and [PeerJS](https://peerjs.com/)
