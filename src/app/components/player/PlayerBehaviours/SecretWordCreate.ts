import { Letter } from "src/app/models/letter";
import { Player } from "src/app/models/player";
import { PlayerComponent } from "../player.component";
import { IPlayerBehaviour } from "./IPlayerBehaviour";

export class SecretWordCreate implements IPlayerBehaviour {
  player: Player;

  constructor(player: Player) {
    this.player = player;
  }

  LetterSelected(letter: { letter: Letter, selected: boolean }) {
    if (letter.selected) {
      this.player.secretWord.push(letter.letter);
    }
    else {
      const index: number = this.player.secretWord.indexOf(letter.letter);
      this.player.secretWord.splice(index, 1);
    }
  }

  LetterReset() {
    this.player.secretWord = [];
  }

  Confirm()
  {
    this.player.secretWordOk = true;
  }
}