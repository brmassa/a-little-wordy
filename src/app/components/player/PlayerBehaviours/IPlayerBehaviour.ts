import { Letter } from "src/app/models/letter";

export interface IPlayerBehaviour {
  LetterSelected(letter: { letter: Letter, selected: boolean }): void;
  LetterReset(): void;
  Confirm(): void;
}