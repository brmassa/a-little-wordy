import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Letter } from 'src/app/models/letter';
import { Player } from 'src/app/models/player';
import { GameStage } from "../../models/gameStage";
import { IPlayerBehaviour } from './PlayerBehaviours/IPlayerBehaviour';
import { SecretWordCreate } from './PlayerBehaviours/SecretWordCreate';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent {

  @Input()
  playerId: number = 0;

  @Input()
  player: Player = new Player();

  @Input()
  playerCurrent: number = 0;

  @Input()
  playerHuman: number = 0;

  @Input()
  set gameStage(value: GameStage) {
    this.GameStageChanged(value);
  };

  @Output() confirmed = new EventEmitter();

  _gameStage: GameStage = GameStage.none;

  get isLetterSelectable() {
    return this._gameStage == GameStage.secretWordCreate
      && !this.player.secretWordOk
      && this.playerId == this.playerHuman;
  };

  playerBehaviour: IPlayerBehaviour;

  constructor() {
    this.playerBehaviour = new SecretWordCreate(this.player);
  }

  GameStageChanged(value: GameStage) {
    this._gameStage = value;

    if (value == GameStage.secretWordCreate) {
      this.playerBehaviour = new SecretWordCreate(this.player);
    }
  }

  PlayerClass(player: number) {
    return this.playerHuman == player;
  }

  IsPlayerCurrent(player: number) {
    return player == this.playerCurrent;
  }

  LetterSelected(letter: { letter: Letter, selected: boolean }) {
    this.playerBehaviour.LetterSelected(letter);
  }

  LetterReset() {
    this.playerBehaviour.LetterReset();
  }

  Confirm() {
    this.playerBehaviour.Confirm();
    this.confirmed.emit();
  }

  get ConfirmSecretWord() {
    return this._gameStage == GameStage.secretWordCreate
      && this.player.secretWord.length > 0
      && !this.player.secretWordOk;
  }
}
