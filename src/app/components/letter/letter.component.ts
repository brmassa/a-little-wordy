import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Letter } from 'src/app/models/letter';

@Component({
  selector: 'app-letter',
  templateUrl: './letter.component.html',
  styleUrls: ['./letter.component.css']
})
export class LetterComponent {

  @Input()
  letter: Letter = new Letter();

  @Input()
  set IsSelectable(value: boolean) {
    this.isSelectable = value;

    if (!this.isSelectable) {
      this.isSelected = false;
    }
  };

  @Output() selected = new EventEmitter<{ letter: Letter, selected: boolean }>();

  isSelectable: boolean = true;

  isSelected = false;
  isUsed = false;

  click() {
    if (this.isSelectable) {
      this.isSelected = !this.isSelected;
      this.selected.emit({ letter: this.letter, selected: this.isSelected });
    }
  }
}
