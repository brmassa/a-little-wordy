import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
var parcelRequire;
import Peer from 'peerjs';

@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('popOverState', [
      state('true', style({
      })),
      state('false', style({
        'display': 'none',
        'height': 0,
        'opacity': 0
      })),
      transition('* => *', animate('300ms ease-out')),
    ])
  ]
})
export class AppComponent implements OnInit {
  peer: Peer;
  name: string = "";
  connectId: string = "";

  start = false;

  TopBarShow = true;

  constructor() {
    this.peer = new Peer();
  }

  ngOnInit(): void {
    this.name = "Player " + Math.floor(Math.random() * 1000000).toString();
  }

  async gameStart() {
    this.start = false;
    this.connectId = this.connectId.replace(/\s/g, "");
    this.start = true;
    this.TopBarShow = false;
  }
}
