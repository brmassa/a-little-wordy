import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Peer from 'peerjs';
import { Card } from 'src/app/models/card';
import { Game } from 'src/app/models/game';
import { Player } from 'src/app/models/player';
import { GameNewParams } from 'src/app/models/gameNewParams';
import { GameStage } from 'src/app/models/gameStage';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  @Input()
  name: string = "";

  @Input()
  connectId: string = "";

  @Input()
  peer: Peer = new Peer();

  conn: Peer.DataConnection | undefined;

  game: Game;

  playerHuman = 0;

  get GameId() { return this.connectId ? this.connectId : this.peer.id; }

  constructor(protected http: HttpClient) {
    this.game = new Game();
  }

  GameStage = GameStage;

  gameParams = new GameNewParams();

  ngOnInit(): void {

    if (this.connectId) {
      console.log("connecting to:", this.connectId);
      this.conn = this.peer.connect(this.connectId);
      if (!this.conn) return;

      this.conn.on('open', () => {
        this.Listen();
        console.log('connected');
        this.conn?.send({
          id: this.peer.id,
          type: 'CmdConnect',
          value: this.name
        });
      });
    }
    else {
      this.GameInit();
      this.peer.on('connection', (conn) => {
        this.conn = conn;
        this.Listen();
      });
    }
  }

  Listen(): void {
    this.conn?.on('data', (data) => {
      console.log('Received', data);
      switch (data.type) {
        case 'CmdConnect':
          this.CmdConnect(data.id, data.value);
          break;
        case 'CmdPlayerSetName':
          this.CmdPlayerSetName(data.id, data.value);
          break;
        case 'CmdGameSet':
          this.CmdGameSet(data.id, data.value);
          break;
        case 'CmdGameStageSet':
          this.CmdGameStageSet(data.id, data.value);
          break;
        case 'CmdSecretWordChosen':
          this.CmdSecretWordChosen(data.id, data.value);
          break;
        case 'CmdMatchSet':
          this.CmdMatchSet(data.id, data.value);
          break;
        default:
          break;
      }
    });
  }

  CmdConnect(id: string, name: string) {
    var playerId = this.getPlayerId(id);
    this.game.players[playerId] = this.PlayerNew(name);

    this.conn?.send({
      id: this.peer.id,
      type: 'CmdGameSet',
      value: this.game
    });
  }

  CmdPlayerSetName(id: string, name: string): void {
    var playerId = this.getPlayerId(id);
    this.game.players[playerId].name = name;
  }

  CmdSecretWordChosen(id: string, player: Player): void {
    var playerId = this.getPlayerId(id);
    this.game.players[playerId] = player;
    this.GameStageSecretWordCheck();
  }

  CmdGameSet(id: string, game: Game): void {
    this.playerHuman = 1;
    this.game = game;
  }

  CmdGameStageSet(id: string, stage: GameStage) {
    this.game.stage = stage;
  }

  CmdMatchSet(id: string, game: Game) {
    this.game = game;
  }

  async CardsGenerate() {
    return firstValueFrom(this.http.get<Card[]>("/assets/cards.json"))
      // return this.http.get<Card[]>("/assets/cards.json").toPromise()
      .then((cardsData) => {
        if (!cardsData) return;
        this.game.cards = [];
        var cardSpecial: Card[] = [];
        var cardNormal: Card[] = [];
        cardsData.forEach(cardData => {
          if (cardData.special)
            cardSpecial.push(cardData as Card);
          else
            cardNormal.push(cardData as Card);
        });
        this.CardsGenerateType(cardNormal, this.gameParams.cardNormal);
        this.CardsGenerateType(cardSpecial, this.gameParams.cardSpecial);
      });
  }

  CardsGenerateType(cards: Card[], quantity: number): void {
    cards = cards.sort(() => (Math.random() > .5) ? 1 : -1);
    cards = cards.slice(0, quantity);
    this.game.cards = [...this.game.cards, ...cards];
  }

  GameInit() {
    this.playerHuman = 0;
    this.game = new Game();
    this.game.players = [this.PlayerNew(this.name)];
  }

  GameStageSet(stage: GameStage) {
    this.game.stage = stage;

    this.conn?.send({
      id: this.peer.id,
      type: 'CmdGameStageSet',
      value: this.game.stage
    });
  }

  GameStageSecretWordConfirmed() {
    this.conn?.send({
      id: this.peer.id,
      type: 'CmdSecretWordChosen',
      value: this.game.players[this.playerHuman]
    });
    this.GameStageSecretWordCheck();
  }

  GameStageSecretWordCheck() {
    if (this.game.players[0].secretWordOk && this.game.players[1].secretWordOk) {
      this.GameStageSet(GameStage.cardSelection);
    }
  }

  LettersGenerate() {
    var vowels = "aeiou";
    var consonants = "qwrtypsdfghjklzxcvbnm";
    this.game.players.forEach(player => {
      player.letters = [];
      this.LettersGeneratePerPlayer(player, vowels, this.gameParams.playerVowels);
      this.LettersGeneratePerPlayer(player, consonants, this.gameParams.playerConsonants);
    });
  }

  LettersGeneratePerPlayer(player: Player, lettersString: string, quantity: number): void {
    var letters = lettersString.split(new RegExp(""));
    for (let index = 0; index < quantity; index++) {
      const element = Math.floor(Math.random() * (letters.length));
      player.letters.push({
        id: "",
        letter: letters[element],
        vowel: this.isVowel(letters[element])
      });
    }
  }

  async MatchInit() {
    await this.CardsGenerate();
    this.LettersGenerate();

    this.conn?.send({
      id: this.peer.id,
      type: 'CmdMatchSet',
      value: this.game
    });

    this.GameStageSet(GameStage.secretWordCreate);
  }

  PlayerNew(name: string): Player {
    return {
      name: name,
      letters: [],
      secretWord: [],
      secretWordOk: false,
      points: 0
    }
  }

  private vowels = ["a", "e", "i", "o", "u"];

  isVowel(letter: string): boolean { return this.vowels.includes(letter); }

  getPlayerId(playerId: string) {
    return Math.abs(this.playerHuman - (playerId == this.peer.id ? 0 : 1));
  }
}
