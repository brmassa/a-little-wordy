import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/app/app.component';
import { GameComponent } from './components/game/game.component';
import { CardComponent } from './components/card/card.component';
import { LetterComponent } from './components/letter/letter.component';
import { PlayerComponent } from './components/player/player.component';

@NgModule({
  declarations: [
    GameComponent,
    CardComponent,
    LetterComponent,
    PlayerComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
