import { TestBed } from '@angular/core/testing';

import { ALittleWordyService } from './a-little-wordy.service';

describe('ALittleWordyService', () => {
  let service: ALittleWordyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ALittleWordyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
