import { Card } from "./card";
import { Player } from "./player";
import { GameStage } from "./gameStage";

export class Game {
  public players: Player[] =[];
  public playerCurrent: number = 0;
  public roundCurrent: number = 0;
  public cards: Card[] = [];
  public stage: GameStage = GameStage.none;
}
