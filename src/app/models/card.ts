export class Card {
  public title: string = "";
  public points: number = 1;
  public special: boolean = false;
  
  public imageSrc: string = "";
  
  public powerTitle: string = "";
  public powerDescription: string = "";
  public power: string = "";
}
