
export class GameNewParams {
  public playerVowels: number = 4;
  public playerConsonants: number = 7;
  public cardSpecial: number = 4;
  public cardNormal: number = 4;
}
