export enum GameStage {
  none,
  secretWordCreate,
  cardSelection,
  cardResolution,
  secretWordGuess,
  gameOver,
}
