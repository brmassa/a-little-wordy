export class Letter {
  public id: string = "";
  public letter: string = "";
  public vowel: boolean = false;
}
