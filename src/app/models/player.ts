import { Letter } from "./letter";

export class Player {
  public name = "";
  public letters: Letter[] = [];
  public secretWord: Letter[] = [];
  public secretWordOk: boolean = false;
  public points: number = 0;
}
